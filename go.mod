module teafolio

go 1.19

require (
	github.com/BurntSushi/toml v0.3.1
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a
)

require github.com/yuin/goldmark v1.7.0 // indirect
