# Dockerfile for production Teafolio deployments

FROM golang:1.22-alpine AS builder

WORKDIR /app
COPY . .
RUN go build -ldflags "-s -w" && chmod +x teafolio

FROM alpine:latest

WORKDIR /app
COPY --from=builder /app/teafolio /app/teafolio

ENTRYPOINT [ "/app/teafolio" ]
