package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"regexp"
	"sync"
	"teafolio/gitea"

	"github.com/BurntSushi/toml"
)

type Config struct {
	BindTo string
	Gitea  struct {
		URL, Org, Token string
		MaxConnections  int64
	}
	Redirect map[string]string
	Template struct {
		AppName             string
		HomepageHeaderHTML  string
		CustomLogoPngBase64 string
	}
	OverrideOrder map[string]int
}

type Application struct {
	cfg Config

	rxRepoPage, rxRepoImage *regexp.Regexp

	gitea *gitea.APIClient

	reposMut               sync.RWMutex
	reposCache             []gitea.Repo // Sorted by recently-created-first
	reposCacheByName       map[string]int
	reposAlphabeticalOrder map[string]int
}

func main() {
	app := Application{
		rxRepoPage:  regexp.MustCompile(`^/([^/]+)/?$`),
		rxRepoImage: regexp.MustCompile(`^/:banner/([^/]+)/?$`),
	}

	configFile := flag.String(`ConfigFile`, `config.toml`, `Configuration file in TOML format`)
	flag.Parse()

	_, err := toml.DecodeFile(*configFile, &app.cfg)
	if err != nil {
		log.Fatalf("toml.DecodeFile: %s", err.Error())
	}

	// Create Gitea API client
	app.gitea = gitea.NewAPIClient(app.cfg.Gitea.URL, app.cfg.Gitea.Org, app.cfg.Gitea.Token, app.cfg.Gitea.MaxConnections)

	// Sync worker
	go app.syncWorker(context.Background())

	log.Printf("Starting web server on [%s]...", app.cfg.BindTo)
	log.Fatal(http.ListenAndServe(app.cfg.BindTo, &app))
}
