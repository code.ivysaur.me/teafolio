package main

import (
	"fmt"
	"html"
	"log"
	"net/http"
)

func (this *Application) Templatepage(w http.ResponseWriter, r *http.Request, pageDesc, extraHead string, cb func()) {

	pageTitle := this.cfg.Template.AppName
	if pageDesc != "" {
		pageTitle = pageDesc + ` | ` + pageTitle
	}

	w.Header().Set(`Content-Type`, `text/html; charset=UTF-8`)
	w.WriteHeader(200)
	fmt.Fprint(w, `<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta charset="UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>`+html.EscapeString(pageTitle)+`</title>
		`+extraHead+`
		<link rel="shortcut icon" href="/static/logo.png" type="image/png">
		<link rel="apple-touch-icon" href="/static/logo.png" type="image/png">
		<link type="text/css" rel="stylesheet" href="/static/style.css">
	</head>
	<body>
		<div id="container">
			<div id="content">
				<h1><a href="/"><div id="logo"></div>`+html.EscapeString(this.cfg.Template.AppName)+`</a></h1>
	`)
	cb()
	fmt.Fprint(w, `
	</body>
	<script type="text/javascript" src="/static/site.js"></script>
</html>
`)

}

func (this *Application) internalError(w http.ResponseWriter, r *http.Request, err error) {
	log.Printf("%s %s: %s", r.Method, r.URL.Path, err)
	http.Error(w, "An internal error occurred.", 500)
}

func (this *Application) Delay(w http.ResponseWriter, r *http.Request) {
	this.Templatepage(w, r, "Loading...", "", func() {
		fmt.Fprintf(w, `
		<h2>Loading, please wait...</h2>
		
		<meta http-equiv="refresh" content="5">
		`)
	})
}
