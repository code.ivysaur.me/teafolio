package main

import (
	"fmt"
	"html"
	"net/http"
	"net/url"
	"strings"
)

func (this *Application) Homepage(w http.ResponseWriter, r *http.Request) {

	this.reposMut.RLock()
	defer this.reposMut.RUnlock()

	if len(this.reposCache) == 0 {
		// We haven't loaded the repositories from Gitea yet
		this.Delay(w, r)
		return
	}

	// Ready for template

	this.Templatepage(w, r, "", "", func() {
		fmt.Fprint(w, `
		`+this.cfg.Template.HomepageHeaderHTML+`		
		<select id="sortorder" style="float:right;">
			<option value="data-sort-al">Alphabetical</option>
			<option value="data-sort-ls">Lifespan</option>
			<option value="data-sort-ct" selected>Recent projects</option>
			<option value="data-sort-mt">Recent updates</option>
		</select>
		
		<h2>Projects <small>(`+fmt.Sprintf("%d", len(this.reposCache))+`)</small></h2>
		<table id="projtable-main" class="projtable">
`)
		for repoIdx, repo := range this.reposCache {
			pageHref := html.EscapeString(`/` + url.PathEscape(repo.Name))

			normalisedDesc := repo.Description
			normalisedDesc = strings.TrimRight(repo.Description, `.`)
			if len(normalisedDesc) > 0 {
				// Lowercase the first letter of the description, unless it starts with an acronym (all letters uppercase first word) or CamelCase word
				firstWord := strings.SplitN(normalisedDesc, " ", 2)[0]
				isAcronymOrCamelCase := len(firstWord) > 1 && (firstWord[1:] != strings.ToLower(firstWord[1:]))

				if !(isAcronymOrCamelCase || firstWord == `Go`) {
					normalisedDesc = strings.ToLower(normalisedDesc[0:1]) + normalisedDesc[1:]
				}

				// Add leading `<COMMA><SPACE>` to separate from the repo title
				normalisedDesc = `, ` + normalisedDesc
			}

			rowClass := ""
			for _, topic := range repo.Topics {
				rowClass += `taggedWith-` + topic + ` `
			}

			fmt.Fprint(w, `
			<tr
				class="`+html.EscapeString(rowClass)+`"
				data-sort-al="`+fmt.Sprintf("-%d", this.reposAlphabeticalOrder[repo.Name])+`"
				data-sort-ls="`+fmt.Sprintf("%.0f", repo.NewestCommit.Sub(repo.GiteaCreated).Seconds())+`"
				data-sort-ct="`+fmt.Sprintf("-%d", repoIdx)+`"
				data-sort-mt="`+fmt.Sprintf("%d", repo.NewestCommit.Unix())+`"
			>
				<td>
					<a href="`+pageHref+`"><img class="homeimage" loading="lazy" src="`+html.EscapeString(`/:banner/`+url.PathEscape(repo.Name))+`"></div></a>
				</td>
				<td>
					<strong>`+html.EscapeString(repo.Name)+`</strong>`+html.EscapeString(normalisedDesc)+`
					<a href="`+pageHref+`" class="article-read-more">more...</a>
					<br>
					<small>
			`)
			for _, topic := range repo.Topics {
				fmt.Fprint(w, `<a class="tag tag-link" data-tag="`+html.EscapeString(topic)+`">`+html.EscapeString(topic)+`</a> `)
			}
			fmt.Fprint(w, `
					</small>
				</td>
			</tr>
			`)
		}
		fmt.Fprint(w, `
		</table>
		`)
	})
}
