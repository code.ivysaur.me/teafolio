package main

import (
	"net/http"
)

func (this *Application) Bannerpage(w http.ResponseWriter, r *http.Request, repoName string) {

	this.reposMut.RLock()
	defer this.reposMut.RUnlock()

	ridx, ok := this.reposCacheByName[repoName]
	if !ok {
		w.Header().Set(`Location`, `/static/no_image.png`)
		w.WriteHeader(301)
		return
	}

	images := this.reposCache[ridx].Images
	if len(images) == 0 {
		w.Header().Set(`Location`, `/static/no_image.png`)
		w.WriteHeader(301)
		return
	}

	w.Header().Set(`Location`, images[0].RawURL)
	w.WriteHeader(301)
}
