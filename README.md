# teafolio

Teafolio is a web-based portfolio frontend for a Gitea server.

Compared to the earlier [codesite](https://code.ivysaur.me/codesite/) project, the repository list and detailed information is loaded live from a Gitea server.

Written in Go

## Usage

1. Compile the binary: `go build`
2. Modify the sample `config.toml` file to point to your Gitea instance
	- `teafolio` will look for `config.toml` in the current directory, or, you can supply a custom path with `-ConfigFile`
3. Deploy binary + `static/` directory to webserver

### Production (Docker)

```bash
docker build -t teafolio:latest .
docker run --restart=always -d -p 5656:5656 -v $(pwd)/config.toml:/app/config.toml teafolio:latest
```

### Production (Dokku)

By default, Dokku will proxy HTTP on port 5000.

```bash
dokku apps:create teafolio
dokku storage:mount teafolio /srv/teafolio-dokku/config.toml:/app/config.toml
```

## CHANGELOG

2024-03-19 v1.4.1
- Add cooldown for failed markdown API calls
- Fix missing extra html elements when using fallback markdown renderer

2024-03-19 v1.4.0
- Support using application token for Gitea API
- Add fallback to internal markdown renderer if Gitea's API is unavailable
- Add fallback to Gitea page redirect if there are any errors building repo page
- Embed static web assets in binary (requires Go 1.16+)

2022-12-31 v1.3.1
- Fix missing images on homepage
- Fix an issue with wrong mtime comparisons for updated repositories

2022-12-31 v1.3.0
- Add `OverrideOrder` configuration option to insert repos at specific positions
- Cache target image URLs to improve homepage load performance

2021-04-12 v1.2.1
- Exclude 'build:success' tag from article-only repositories, and re-host the image locally
- Also search root repo directory for images
- Enhance mobile viewport and table styling

2020-11-19 v1.2.0
- Cache homepage repositories, sync changes in the background
- Consider the updated time to be the most recent commit, not the Gitea repository metadata update field
- Add extra logging to startup

2020-11-08 v1.1.1
- Fix an issue with newer versions of Gitea that paginate repoistory list responses
- Fix an issue with blocking semaphores for a cancelled network request

2020-05-24 v1.1.0
- Support limiting the number of concurrent API requests to Gitea
- Display total number of projects
- Fix cosmetic issues with page background image, page height, and margins around thumbnails

2020-05-24 v1.0.1
- Remove image dependency from static files
- Fix a cosmetic issue with `h2`/`h3` margins

2020-05-05 v1.0.0
- Initial release
