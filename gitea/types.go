package gitea

import (
	"strings"
	"time"
)

type Repo struct {
	Name         string    `json:"name"`
	Description  string    `json:"description"`
	GiteaCreated time.Time `json:"created_at"`
	GiteaUpdated time.Time `json:"updated_at"`

	// NewestCommit is populated via PopulateCommitInfo().
	NewestCommit time.Time
	// Topics is populated via PopulateTopics().
	Topics []string
	// Images are populated via PopulateImages().
	Images []ReaddirEntry
}

type ContentsResponse struct {
	Content []byte `json:"content"` // Assume base64 "encoding" parameter in Gitea response, and use Go's auto decode
}

type TopicsResponse struct {
	Topics []string `json:"topics"`
}

type ReaddirEntry struct {
	Name   string `json:"name"`
	Path   string `json:"path"`
	Size   int    `json:"size"`
	RawURL string `json:"download_url"`
}

func (rde ReaddirEntry) isImage() bool {
	return strings.HasSuffix(rde.Name, `.png`) || strings.HasSuffix(rde.Name, `.jpg`) || strings.HasSuffix(rde.Name, `.jpeg`)
}

type MarkdownRequest struct {
	Context string
	Mode    string
	Text    string
	Wiki    bool
}

type BranchCommit struct {
	ID        string    `json:"id"`
	Message   string    `json:"message"`
	Timestamp time.Time `json:"timestamp"`
}

type Branch struct {
	Name   string       `json:"name"`
	Commit BranchCommit `json:"commit"`
}

type AuthorInfo struct {
	Name  string    `json:"name"`
	Email string    `json:"email"`
	Date  time.Time `json:"date"`
}

type CommitListEntryCommit struct {
	Message   string     `json:"message"`
	Author    AuthorInfo `json:"author"`
	Committer AuthorInfo `json:"committer"`
}

type CommitListEntry struct {
	ID     string                `json:"sha"`
	Commit CommitListEntryCommit `json:"commit"`
}
